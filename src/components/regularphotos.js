import React, { Component } from 'react';
import styles from '../stylesheets/meme.css';
import 'react-photoswipe/lib/photoswipe.css';

import {PhotoSwipeGallery} from 'react-photoswipe';


class RegularPhotos extends Component {
    constructor(){
        super()
 
        this.items = [
            {
                src: require("../imgs/photos/usa.jpg"),
                w: 720,
                h: 540,
                title: 'Red, White, and Blue Day'
            },
            {
                src: require("../imgs/photos/christmas.jpg"),
                w: 720,
                h: 540,
                title: 'Ugly Christmas Sweaters Day'
            },
            {
                src: require("../imgs/photos/pinkandsparklyday.jpg"),
                w: 720,
                h: 540,
                title: 'Pink and Sparkly Day'
            },
            {
                src: require("../imgs/photos/swimgear.jpg"),
                w: 720,
                h: 540,
                title: 'Swim Gear Day'
            },
        ];
 
        this.options = {
        //http://photoswipe.com/documentation/options.html 
        };
 
        this.handleClose = () => {
            isOpen: false
        };
    }
    render() {
        return (
            <div className="photos">
                <h1> Gallery </h1>
                <div className="media-container">
                    <h2> Images </h2>
                    <PhotoSwipeGallery isOpen={false} items={this.items} options={this.options} onClose={this.handleClose}/>
                </div>
            </div>
        );
    }
}

export default RegularPhotos;
