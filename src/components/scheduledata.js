import React, { Component } from 'react';
import styles from '../stylesheets/schedule.css';

let content = [
'12/01/17',	'At Marshall',
'12/08/17', 'Jeb Stuart, Edison',
'12/15/17', 'At Oakton, Mt. Vernon',
'1/12/18', 'At Lee',
'1/13/18', 'At Falls Church',
'1/19/18', 'At Wakefield',
'1/25/18', 'National District Dive Championships',
'1/26/18', 'National District Swim Championships Prelims',
'1/27/18', 'National District Swim Championships Finals',
'1/31/18', 'Region 5C Dive Championships',
'2/01/18', 'Region 5c Swim Championships',
'2/16/18', 'Virginia 5A State Championships',
]

class ScheduleData extends Component {
    constructor(){
        super();

        this.listItems = content.map((event) =>
            <h2>{event}</h2>
        );
    }
    render() {
        return (
            <div id="schedule">
                <h1> Schedule </h1>
                <div id="event">
                {this.listItems}
                </div>
            </div>
        );
    }
}

export default ScheduleData;
