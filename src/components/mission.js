import React, { Component } from 'react';
import Slider from 'react-slick';
import styles from '../stylesheets/mission.css';

class Mission extends Component {
  render() {
    return (
        <div id="mission">
          <div className="imgs">
            <Slider autoplay={true} dots={true} draggable={false} easing='ease'>
              <div className="img">
                <img src={require("../imgs/regionalteam.jpg")}/>
                <h3> Regional Team </h3>
              </div>
              <div className="img">
                <img src={require("../imgs/coaches.jpg")}/>
                <h3> Coaches </h3>
              </div>
              <div className="img">
                <img src={require("../imgs/captains.jpg")}/>
                <h3> Captains </h3>
              </div>
            </Slider>
          </div>
            
          <div className="text">
            <h2>"TJSD is love, TJSD is life"</h2>
            <span>WE ARE TJSD</span>
            <p>We are a cohort of Jefferson student-athletes committed to swimming and diving and beating everyone. Like a pride of lions on the prowl, TJSD trains as a team, competes as a team, and is always hungry for more. And like the mythical Tantalus, son of Zeus, even though we are surrounded by water, our thirst is unquenchable. Our mission is to represent our school onto the pool deck, compete with the utmost qualities of good sportsmanship and phelpsian domination, whilst fighting for Science and Tech and the Glory of TJ.</p>
            <h4>Ian Handerhan</h4>
          </div>
        </div>
    );
  }
}

export default Mission;
