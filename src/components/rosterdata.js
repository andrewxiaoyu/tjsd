import React, { Component } from 'react';
import styles from '../stylesheets/rosterdata.css';

let roster = [
  "Sylvia Tan",
  "Vivian Lin",
  "Kaitlin Han",
  "Sarah Wang",
  "Emma Small",
  "Caroline Jin",
  "Melanie Le",
  "Victoria Lu",
  "Alice Fontaine",
  "Lynne Bai",
  "Cynthia Hu",
  "Eva DeCesare",
  "Sophia Wang",
  "Hana Wang",
  "Sydney Harrington",
  "Natalie Martin",
  "Militsa Sotirova",
  "Meghna Sharma",
  "Zoe Van Winckel",
  "Jenny Li",
  "Fiona Carcani",
  "Allison Shupp",
  "Caitlin Sughrue",
  "Cassie Quach",
  "Anais Beauvais",
  "Jennifer Zheng",
  "Tiffany Ji",
  "Summer Wang",
  "Elizabeth Louie",
  "Grace Chong",
  "Sam Joyner",
  "Jonathan Pollock",
  "Daniel Li",
  "Anthony Wong",
  "Abhishek Bazaz",
  "Evan Zhang",
  "Jacob Blindenbach",
  "Henry Zhang",
  "Edward Shen",
  "Nithin Bagal",
  "Ben Sharrer",
  "David DiMeglio",
  "Jeffrey Gerber",
  "Zachary Kwon",
  "Anthony Murphy-Neilson",
  "Kevin Gu",
  "Dylan Jones",
  "Ethan Kwack",
  "Roger Zeng",
  "Andrew West",
  "Brandon Fogg",
  "Alex Liu",
  "Joshua Boisvert",
  "James Frucht",
  "Dan Beckstrand",
  "Phillip Pan",
  "Jason He",
  "Aditya Kumar",
  "Jay Misener",
  "Charles Wang",
  "Jerry Li",
  "Jason Xu",
]

class RosterData extends Component {
  constructor(){
    super();
    roster.sort();
    this.listItems = roster.map((name) =>
        <p>{name}</p>
    );
  }
  render() {
    return (
      <div id="rosterdata">
        <h1> Roster </h1>
        <div id="roster">
          {this.listItems}
        </div>
      </div>
    );
  }
}

export default RosterData;
