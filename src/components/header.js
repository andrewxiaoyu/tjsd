import React, { Component } from 'react';
import { Link } from "react-router-dom";

import styles from '../stylesheets/header.css';

class Header extends Component {
  render() {
    return (
      <div id="header">
        <Link to="/roster">
          <button>Roster</button>
        </Link>
        <Link to="/">
          <img src={require("../imgs/logo.png")} />
        </Link>
        <Link to="/schedule">
          <button>Schedule</button>
        </Link>
      </div>
    );
  }
}

export default Header;
