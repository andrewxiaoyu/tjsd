import React, { Component } from 'react';
import styles from '../stylesheets/footer.css';

class Footer extends Component {
  render() {
    return (
        <div id="footer">
            <h4>Made with &hearts;</h4>
        </div>
    );
  }
}

export default Footer;
