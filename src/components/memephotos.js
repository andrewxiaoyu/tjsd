import React, { Component } from 'react';
import styles from '../stylesheets/meme.css';
import 'react-photoswipe/lib/photoswipe.css';

import {PhotoSwipeGallery} from 'react-photoswipe';

function getRandomSubarray(arr, size) {
    var shuffled = arr.slice(0), i = arr.length, min = i - size, temp, index;
    while (i-- > min) {
        index = Math.floor((i + 1) * Math.random());
        temp = shuffled[index];
        shuffled[index] = shuffled[i];
        shuffled[i] = temp;
    }
    return shuffled.slice(min);
}

class MemePhotos extends Component {
    constructor(){
        super()
 
        this.items = [];
        for(var a = 1; a<=53; a++){
            this.items.push(a);
        }
        this.items = getRandomSubarray(this.items, 5);

        this.imgs = [];

        for (var b = 0; b < this.items.length; b++) {
            let i = {
                src: require("../imgs/memes/"+this.items[b]+".jpg"),
                w: 540,
                h: 540,
            };
            this.imgs.push(i);
        }

        // this.imageItems = this.imgs.map((item) =>
        //     <img src={item.src} />
        // );
 
        this.options = {
        //http://photoswipe.com/documentation/options.html 
        };
 
        this.handleClose = () => {
            isOpen: false
        };
    }
    render() {
        return (
            <div className="photos">
                <h1> Memes </h1>
                <div className="media-container">
                    <h2> Videos </h2>
                    <div className="videoreel">
                        <video width='320' height='240' controls><source src={require('../videos/imgladyoucame.mp4')} type='video/mp4'/ ></video>
                    </div>
                </div>
                <div className="media-container">
                    <h2> Images </h2>
                    <PhotoSwipeGallery isOpen={false} items={this.imgs} options={this.options} onClose={this.handleClose}/>
                </div>
            </div>
        );
    }
}

export default MemePhotos;
