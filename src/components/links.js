import React, { Component } from 'react';
import { Link } from "react-router-dom";

import styles from '../stylesheets/links.css';

class Links extends Component {
  render() {
    return (
        <div id="links">
            <Link to="/photos">
              <button>Photos</button>
            </Link>
            <Link to="/memes">
              <button>Memes</button>
            </Link>
        </div>
    );
  }
}

export default Links;
