import React, { Component } from 'react';
import styles from '../stylesheets/notification.css';

class Notification extends Component {
  render() {
    return (
      <div id="notification">
        <h4><span class="bold">2/25</span> - Donate to the Coaches Fund <a href="http://www.signupgenius.com/go/30e0e49aeab2a1-tjswim" target="_blank">Here</a></h4>
        <h4><span class="bold">2/12</span> - TJSD Senior Night</h4>
      </div>
    );
  }
}

export default Notification;
