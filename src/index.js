import React from 'react';
import ReactDOM from 'react-dom';
import { Route, HashRouter } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

import Home from './views/home';
import Roster from './views/roster';
import Schedule from './views/schedule';
import Memes from './views/memes';
import Photos from './views/photos';
import styles from "./stylesheets/style.css";

ReactDOM.render(
    <HashRouter basename={process.env.PUBLIC_URL}>
        <div>
            <Route exact path='/' component={Home} />
            <Route exact path='/roster' component={Roster} />
            <Route exact path='/schedule' component={Schedule} />
            <Route exact path='/photos' component={Photos} />
            <Route exact path='/memes' component={Memes} />
        </div>
    </HashRouter>,
    document.getElementById('root')
);
registerServiceWorker();