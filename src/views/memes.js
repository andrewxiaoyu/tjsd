import React, { Component } from 'react';

import Header from '../components/header';
import MemePhotos from '../components/memephotos';
import Footer from '../components/footer';

class Memes extends Component {
  render() {
    return (
      <div>
        <Header />
        <MemePhotos />
        <Footer />
      </div>
    );
  }
}

export default Memes;