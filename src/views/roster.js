import React, { Component } from 'react';

import Header from '../components/header';
import RosterData from '../components/rosterdata';
import Footer from '../components/footer';

class Roster extends Component {
  render() {
    return (
      <div>
        <Header />
        <RosterData />
        <Footer />
      </div>
    );
  }
}

export default Roster;