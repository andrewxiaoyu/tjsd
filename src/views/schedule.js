import React, { Component } from 'react';

import Header from '../components/header';
import ScheduleData from '../components/scheduledata';
import Footer from '../components/footer';

class Schedule extends Component {
  render() {
    return (
      <div>
        <Header />
        <ScheduleData />
        <Footer />
      </div>
    );
  }
}

export default Schedule;