import React, { Component } from 'react';

import Header from '../components/header';
import Notification from '../components/notification';
import Mission from '../components/mission';
import Links from '../components/links';
import Footer from '../components/footer';

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <Notification />
        <Mission />
        <Links />
        <Footer />
      </div>
    );
  }
}

export default Home;
