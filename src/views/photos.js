import React, { Component } from 'react';

import Header from '../components/header';
import RegularPhotos from '../components/regularphotos';
import Footer from '../components/footer';

class Photos extends Component {
  render() {
    return (
      <div>
        <Header />
        <RegularPhotos />
        <Footer />
      </div>
    );
  }
}

export default Photos;